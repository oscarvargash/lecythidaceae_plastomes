# README #

### What is this repository for? ###

* This is the repository for "The chloroplast genome of the Brazil nut (Bertholletia excelsa) and other 23 Lecythidaceae species: structure and genomic resources for phylogenetics and barcoding"

* Here you will find the code associated with the study. The code is organized in folders according to the analysis. Every folder contains a useful readme file.

