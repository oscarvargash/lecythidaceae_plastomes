# this code extracts the caculated likelihood per site
# converts it into a data frame and then makes an average
# the output is a file with the averaged likelihood per site

f_name = 'RAxML_perSiteLLs.nulltest'
import pandas as pd

with open(f_name, 'r') as infile:
    out_name = f_name + '.out'
    ls = []
    for i, line in enumerate(infile):
        if i >= 1:
            d1 = line.split('\t')
            d = d1[1].split(' ')
            ls.append(d)
df = pd.DataFrame(ls)
dft = df.transpose()
dft = dft.convert_objects(convert_numeric=True)
dft.dtypes
dft['mean'] = dft.mean(axis=1)

dft['mean'].to_csv('mean_l_sim.txt', sep='\t', index_col=False)





