d <- getwd()
setwd(d)
dir()

#install.packages('phylotools')
library(phytools)
library(phylotools)

#read the estimated tree
stt <- read.tree("RAxML_bestTree.infile.tre")

#plot the estimated tree
plot(stt)

#test the funtion that permutates trees
stt_sh <- phyloshuffle(stt)

#plot a estimated trees along with a shuffle tree
par(mfcol=c(1,2))
plot(stt)
axisPhylo()
plot(stt_sh)
axisPhylo()

#Create a list of trees with shuffled names
stt_sh = list()
for (i in seq(from = 1, to = 1000)){
  stt_sh[[i]] <- phyloshuffle(stt)
  print(i)
}
#save the permutated trees in a file
outname = "permuted_trees"
lapply(stt_sh, write.tree, file=outname, append=T)

#error checking
#perform a consensus of the shuffled trees
#it should produce a star tree
perm <- read.tree("permuted_trees")

c_tree <- consensus.edges(perm)

plot(c_tree)


