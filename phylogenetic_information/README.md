# README #

### What is this repository for? ###

* This repository contains a variety of code to evaluate phylogenetic information in an alignment of chloroplast genomes (or any other genomic DNA) with the aim of developing barcoding markers with phylogenetic information. 
* Version 2

### How do I get set up? ###

* After obtaining a phylogeny (e.g."RAxML_bestTree.infile.tre"), we are going to compare the loglikelihood of the obtained vs. the average loglikelihood of 1000 permuted trees

* "permute_trees.R" takes the inferred tree from the chloroplast genome alignment "RAxML_bestTree.infile.tre" and perform permutations to create 1000 permuted trees "permuted_trees".

* Calculate the loglikelihood per site of the inferred tree in raxml:
```
raxmlHPC-PTHREADS-AVX -f g -s lecy_cp_gen.phy.reduced -m GTRGAMMA -z RAxML_bestTree.infile.tre -n test 
```
* Calculate the log likelihood per site in each permuted tree:
```
raxmlHPC-PTHREADS-AVX -f G -s lecy_cp_gen.phy.reduced -m GTRGAMMA -z permuted_trees -n nulltest
```
* Modify the output "RAxML_perSiteLLs.test" obtained from the inferred tree using the python scripts "switch_row_col_num.py" and "switch_row_col.py". These modifications allow the data to be read easily in R. The outputs are "RAxML_perSiteLLs.testnumber.out" and "RAxML_perSiteLLs.test.out", respectively.

* Modify the output "RAxML_perSiteLLs.nulltest" (which is compressed) with "mean_likelihood_sim_tree.py" that calculates the average loglikelihood per site of the 1000 trees. The output is "mean_l_sim.txt"

* Now, we are ready to calculate the differences in loglikelihood and make the figures. Execute "CDStest_v2.R", which takes RAxML_perSiteLLs.testnumber.out", "RAxML_perSiteLLs.test.out", and "mean_l_sim.txt" as inputs. This script would make the binned window analysis and would give you the starting coordinate of windows with high phylogenetic information.

* "CDStest_v2.html" contains a report with from running "CDStest_v2.R"
