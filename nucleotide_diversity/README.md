# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains a variety of code to evaluate nucleotide diversity in an alignment of chloroplast genomes (or any other genomic DNA) with the aim of developing barcoding markers. The code makes a graph and calculates those windows that have a score of phylogenetic information over a 95% percentile.
* Version 1

### How do I get set up? ###

* This code could be used with the output of DNASP "dnasp_600_600.txt" (derived from "dnasp_600_50.csv" which was derived from "sliding_window_stepsize600.txt")

or

* You can use an alignment (I added one in the folder "align") as input and calculate nucleotide diversity in R using the package "PogGenome",

* The file "nucleotide_diversity.r" contains the main code.

* The file "nucleotide_diversity.html" contains an example of an output.