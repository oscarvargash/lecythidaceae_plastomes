d <- "/Users/oscar/Box\ Sync/Lecythidaceae_local/plastomes/code/nucleotide_diversity"
setwd(d)
dir()

#install.packages("PopGenome")

# THIS PART OF THE SCRIPT GRAPHS NUCLEOTIDE DIVERSITY USING THE PACKAGE POPGENOME

library(PopGenome)
align <- readData('align', format = 'phylip')
show.slots(align)
align@n.sites
get.sum.data(align)
align600 <- sliding.window.transform(align, width=600, jump=600, type=2, whole.data=T)
align600@region.names
align600nd <- diversity.stats(align600, pi = T)
align600nd@nuc.diversity.within
align600nd@Pi
plot(align600nd@nuc.diversity.within,t='l',main = "600bp window", xaxt='n', xlab = ' ', ylab="Nucleotide diversity")
plot(align600nd@Pi,t='l',main = "600bp window", xlab = ' ', ylab="Nucleotide diversity")

# THIS PART OF THE SCRIPT GRAPHS NUCLEOTIDE DIVERSITY USING THE OUTPUT FROM DNASP

win600 <- read.table('dnasp_600_600.txt', sep='\t', header = T)
head(win600)

pdf("pi_600.pdf",h=5,w=9)
plot(win600$Midpoint, win600$Pi,t='l',main = "", 
     xlab = ' ', ylab="Nucleotide diversity (Pi)", xaxt='n')
abline(v = 95920, lwd=1, col='black')
abline(v = 125229, lwd=1, col='black')
abline(v=36000, lty= 2, lwd=3, col='darkgray')
abline(v=37800, lty= 2, lwd=3, col='darkgray')
abline(v=44400, lty= 2, lwd=3, col='darkgray')
abline(v=45000, lty= 2, lwd=3, col='darkgray')
abline(v=90400, lty= 2, lwd=3, col='darkgray')
abline(v=96000, lty= 2, lwd=3, col='darkgray')
abline(v=132000, lty= 2, lwd=3, col='darkgray')
abline(v=144600, lty= 2, lwd=3, col='darkgray')
abline(v=145200, lty= 2, lwd=3, col='darkgray')
mtext("psbM-trnD", side=3, at=36000, adj=-0.03, padj=0.2, las=3)
mtext("trnE-trnT", side=3, at=37800, adj=-0.03, padj=0.8, las=3)
mtext("psbZ-trnfM", side=3, at=44400, adj=-0.03, padj=0, las=3)
mtext("trnfM-psaB", side=3, at=45000, adj=-0.03, padj=1, las=3)
mtext("petD-rpoA", side=3, at=90400, adj=-0.03, padj=0.3, las=3)
mtext("rpl16-rps3", side=3, at=96000, adj=-0.03, padj=0.5, las=3)
mtext("ccsA-ndhD", side=3, at=132000, adj=-0.03, padj=0.3, las=3)
mtext("ycf1", side=3, at=144600, adj=-0.03, padj=0.5, las=3)
dev.off()

# identify windows falling above the 95% distribution
hist(win600$Pi)
#reg2k <- vector()
q1 <- quantile(win600$Pi, .95, na.rm = TRUE)

win600$Pi[2]
win600$Window[2]
length(win600$Pi)

reg6h <- vector()
for (i in 1:length(win600$Pi)){
  if (win600$Pi[i] > q1){
    print (win600$Window[i])
    print (win600$Pi[i]) #prints the windows above the 95% (9)
  }
}
