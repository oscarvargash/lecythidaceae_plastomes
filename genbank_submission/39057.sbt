Submit-block ::= {
  contact {
    contact {
      name name {
        last "Vargas",
        first "Oscar",
        middle "",
        initials "",
        suffix "",
        title ""
      },
      affil std {
        affil "University of Michigan",
        div "Ecology and Evolutionary Biology",
        city "Ann Arbor",
        sub "MI",
        country "United States of America",
        street "830 North University",
        email "oscarvargash@gmail.com",
        postal-code "48109"
      }
    }
  },
  cit {
    authors {
      names std {
        {
          name name {
            last "Vargas",
            first "Oscar",
            middle "",
            initials "M.",
            suffix "",
            title ""
          }
        },
        {
          name name {
            last "Thomson",
            first "Ashley",
            middle "",
            initials "M.",
            suffix "",
            title ""
          }
        },
        {
          name name {
            last "Dick",
            first "Christopher",
            middle "",
            initials "W.",
            suffix "",
            title ""
          }
        }
      },
      affil std {
        affil "University of Michigan",
        div "Ecology and Evolutionary Biology",
        city "Ann Arbor",
        sub "MI",
        country "United States of America",
        street "830 North University",
        postal-code "48109"
      }
    }
  },
  subtype new
}
Seqdesc ::= pub {
  pub {
    gen {
      cit "unpublished",
      authors {
        names std {
          {
            name name {
              last "Vargas",
              first "Oscar",
              middle "",
              initials "M.",
              suffix "",
              title ""
            }
          },
          {
            name name {
              last "Thomson",
              first "Ashley",
              middle "",
              initials "M.",
              suffix "",
              title ""
            }
          },
          {
            name name {
              last "Dick",
              first "Christopher",
              middle "",
              initials "W.",
              suffix "",
              title ""
            }
          }
        }
      },
      title "The chloroplast genome of the Brazil nut (Bertholletia excelsa)
 and other 23 Lecythidaceae species: structure, genomic resources, and
 phylogenetics"
    }
  }
}
Seqdesc ::= user {
  type str "DBLink",
  data {
    {
      label str "BioProject",
      num 1,
      data strs {
        "SUB2740669"
      }
    },
    {
      label str "BioSample",
      num 1,
      data strs {
        "SAMN07184698"
      }
    }
  }
}
