#!/usr/bin/python

#program to duplicate .src files (every submission needs one)

import pandas as pd
import os

os.chdir('/Users/oscar/Desktop/Lecy_genbank/')
db = pd.read_csv('sbt_database.csv')

#make duplicate of 39070.src for every accesion, we will later modify the accession number

for index, row in db.iterrows():
	lines = []
	with open('39057.src') as infile:		
		for line in infile:
			lines.append(line)
	with open(row['file'], 'w') as outfile:
		for line in lines:
				outfile.write(line)


