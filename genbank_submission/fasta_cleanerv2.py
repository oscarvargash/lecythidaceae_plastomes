#!/usr/bin/python
#Program to clean fasta files from " ."
import os
import fnmatch
#create list of files that need to be cleaned
files = []
for file in os.listdir("/Users/oscar/Desktop/Lecy_genbank"):
    if fnmatch.fnmatch(file, '*.fsa'):
        files.append(file)
        print file
print files

replacements = {' .':''}
# loop to iterate in each file
for i in files:
    lines = []
    with open(i) as infile:
        for line in infile:
            for src, target in replacements.iteritems():
                line = line.replace(src, target)
            lines.append(line)
    with open(i, 'w') as outfile:
        for line in lines:
            outfile.write(line)
        
        
