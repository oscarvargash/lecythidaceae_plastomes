#!/usr/bin/python
#Program to remove the line "locus_tag" and other lines from .tbl files
import os
import fnmatch
#create list of files that need to be cleaned
files = []
for file in os.listdir("/Users/oscar/Desktop/Lecy_genbank"):
    if fnmatch.fnmatch(file, '*.tbl'):
        files.append(file)
        print file
#print files

replacements = {"/":"", "'":""}

for file in files:
    lines = []
    with open(file) as infile:
    		for line in infile:
    		    if 'locus_tag' not in line and 'note' not in line and 'db_xref' not in line and 'protein_id' not in line and 'transl_table' not in line and 'misc_feature' not in line:
    			    for src, target in replacements.iteritems():
    				    line = line.replace(src, target)
    			    if ';' in line:
    			        line = "\t\t\t" + line.split(";")[0].strip() + "\n"
    			    lines.append(line)
    with open(file, 'w') as outfile:
        for line in lines:
            outfile.write(line)
