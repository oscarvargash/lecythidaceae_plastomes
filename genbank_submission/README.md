# README #

### What is this repository for? ###

* Code and instructions for chloroplast submission to genbank
* Version 1

### How do I get set up? ###

* To follow the instructions here you need to have your cp genomes annotated and your project registered in genbank. The genbank registration would provide a bioproject number and a biosample number for every sample. Make sure you have these codes before starting.
* All the scripts were develop to work in python 2

1. export all the annotated genomes from geneiuous by selecting all the files and exporting them into one genbank file that contains all the sequences

2. Open the .gb file with all the sequences in the program MacVector (the free version is enough to do what do we need, do not pay for it!). Export every sequence in 
.tbl (Sequin table) and .fasta.

3. We need the change the extensions of the .fasta to .fsa to comply with the requirements of the tbl2asn software. Do this by using the following bash command:
```
for file in *.fasta; do mv $file ${file//fasta/fsa}; done;
```
4. Make sure every fasta file is clean with only a code or name as the header for every fasta. If it looks like ">39061" it is fine; in my case I got ">39061 ." (see "dirty.fsa" for an example), so I need to remove all the " ." of every file. This can be easily done with a python script: "fasta_cleanerv2.py". You can easily modify the replacements in the file to adjust the script to your needs.

5. Clean the .tbl files as they come with several unnecessary lines with "tbl_cleaningv3.py". See "dirty.tbl" for an example of an uncleaned file. 

6. Create a template .sbt file from https://submit.ncbi.nlm.nih.gov/genbank/template/submission/

7. Create a comma separated file (src_database.csv) with the future names for the .sbt files matching their biosample numbers provided by genbank after registering the bioproject. You need this file for running the script in the next step.

8. Duplicate and assign the correct biosample number to the duplicates by using the "sbt_writerv2.py", which uses "src_database.csv" created in step 7

9. Create a comma separate file with the future of the .src files matching their biosample number provided by genbank after registering the bioproject. See "sbt_database.csv" for an example. You need this file for running the script in the next step.

10. Create a .src with the information for all accessions and duplicate for every submission using "src_duplicator.py"

11. download tbl2asn from https://www.ncbi.nlm.nih.gov/genbank/tbl2asn2/ uncompress it and add it to your preferred location. Make sure you type "chmod +x tbl2asn" to make the program executable.

12. Run tbl2asn (see the explanation for every argument by typing "tbl2asn -". I used "-a r2u" to indicate that any region with two or more "n" represents a gap in the alignment of unknown size. tbl2asn will take as imput all the files associated with the code .sbt .fsa .tbl and .src

* For just one file you can execute:
```
tbl2asn -p. -t 39057.sbt -i 39057.fsa -V vb -l paired-ends -a r2u -j "[gcode=11] [moltype=genomic DNA][location=chloroplast]"
```
* Because we have multiple files with the prefix:
```
for prefix in $(ls *sbt | sed -e 's/\..*$//'); do echo $prefix; done
```
* We can create a for loop to create the .asn file for each accession and a .gbf file that we can later inspect (By for example importing back to geneious):
```
for prefix in $(ls *sbt | sed -e 's/\..*$//'); do tbl2asn -p. -t $prefix.sbt -i $prefix.fsa -V vb -l paired-ends -a r2u -j "[gcode=11] [moltype=genomic DNA][location=chloroplast]"; done
```