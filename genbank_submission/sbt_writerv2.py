#!/usr/bin/python

#program to modify the .sbt file and insert the correct accession number

import pandas as pd
import os

os.chdir('/Users/oscar/Desktop/Lecy_genbank/')
db = pd.read_csv('src_database.csv')

#make duplicate of 39070.src for every accesion, we will later modify the accession number

for index, row in db.iterrows():
	lines = []
	with open('39070.src') as infile:		
		for line in infile:
			lines.append(line)
	with open(row['file'], 'w') as outfile:
		for line in lines:
				outfile.write(line)

#Now we need to change the accession of every duplicate with their respective
#accession number foun in the dataframe by matchiing the file with is accession

for index, row in db.iterrows():
	lines = []
	with open (row['file']) as infile:		
		for line in infile:
			replacements = {'SAMN07184690': row['accession']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(row['file'], 'w') as outfile:
		for line in lines:
				outfile.write(line)

