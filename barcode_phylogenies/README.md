# README #

### What is this repository for? ###

* This repository contains a variety of code to evaluate and compare multiple phylogenetic trees. The code calculates the average bootstrap support for each tree, the Robinson-Foulds distance of each tree. vs a reference, and graphs those results

* Version 1

### How do I get set up? ###

* You would need a collection of trees with a recognizable name pattern. I provide a collection of 19 trees which name files end in "*.fa.tre" these were calculates with RAxML from the "*.phy" files

* A reference tree ("RAxML_bestTree.infile.tre") for the Robinson-Foulds distance calculation.

* the main code is found in "comparing_trees.R"

* the pdfs provided are output examples of the code

* "rf-dist_bootstap.csv" is an output example

* "raxml_bestree_plus_boots_root.tre" is a tree just for reference in case you are curious about comparing the bootstrap support of gene trees against the tree obtained from the whole plastome alignment.
